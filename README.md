Okay...

The outline:

For each lab:
    - For each sequencing dataset:
        - Download sequencing data
        - Map onto every repeat consensus (using repcoverage)
            - This is split into group1, group2, group3, group4
        - Find total mapping reads
        - Turn counts into CPMs
        - Compare with control/input?


Then what? find good/interesting things?


Questions:
- Should we use samtools to remove PCR duplicates?
- Which fastq sets should we download? including 'temp'.?

The problem is that we need to submit over 1000 jobs. The biggest problem is downloading files, probably. Maybe we could use crontab to batch-submit a certain number of jobs at designated intervals.
