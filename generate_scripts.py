#!/usr/bin/env python

import jinja2
import urllib
#import HTMLParser

import subprocess
import shlex
import os.path

download_script_header = \
"""#!/bin/bash

# Request a lot more than an hour of time...
#SBATCH --time=48:00:00
# Use 1 nodes with 16
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH  --mem=16G
# Specify a job name:
#SBATCH -J repcoverage-download
# Specify an output file
#SBATCH -o repcoverage-download.out
#SBATCH -e repcoverage-download.err

"""

if __name__ == '__main__':
    template = jinja2.Template(open('process_dataset_template.sh').read())
    variables = {'fastq_name_gz': 'wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1.fastq.gz',\
                 'lab_name': 'wgEncodeHaibTfbs',\
                 'fastq_name': 'wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1'}
    lab_name = 'wgEncodeHaibTfbs'
    website = 'http://hgdownload-test.cse.ucsc.edu/goldenPath/hg19/encodeDCC/{0}/'.format(lab_name)
    command = 'wget {0}files.txt'.format(website)
    #subprocess.call(shlex.split(command))
    command = 'grep .fastq.gz files.txt | cut -f 1'
    p1 = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    fastq_gz_files = []
    for l in p1.stdout.readlines():
        fastq_gz_files.append(l.strip())
    download_script = open('download_script.sh', 'w')
    download_script.write(download_script_header)
    for fastq_gz in fastq_gz_files:
        # TODO: check if file exists; if file exists, do nothing.
        # check groups 1,2,3,4,5
        fastq_name = fastq_gz.split('.')[0]
        #print fastq_name
        if os.path.exists('output_group1/' + fastq_name + '_cpm.tsv') and \
           os.path.exists('output_group2/' + fastq_name + '_cpm.tsv') and \
           os.path.exists('output_group3/' + fastq_name + '_cpm.tsv') and \
           os.path.exists('output_group4/' + fastq_name + '_cpm.tsv') and \
           os.path.exists('output_group5/' + fastq_name + '_cpm.tsv'):
            #print 'success'
            continue
        variables['fastq_name_gz'] = fastq_gz
        variables['fastq_name'] = fastq_gz.split('.')[0]
        script_name = 'scripts/' + variables['fastq_name'] + '_run.sh'
        download_script.write('wget -O {2} http://hgdownload.cse.ucsc.edu/goldenPath/hg19/encodeDCC/{0}/{1}.fastq\n'\
                .format(lab_name, variables['fastq_name'], fastq_gz))
        download_script.write('gunzip {0}\n'.format(fastq_gz))
        download_script.write('sbatch {0}\n'.format(script_name))
        with open(script_name, 'w') as outfile:
            outfile.write(template.render(variables))
