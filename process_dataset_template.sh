#!/bin/bash

# Request a lot more than an hour of time...
#SBATCH --time=24:00:00
# Use 1 nodes with 16
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH  --mem=16G
# Specify a job name:
#SBATCH -J run_repcoverage-1
# Specify an output file
#SBATCH -o run_repcoverage-{{ fastq_name }}.out
#SBATCH -e run_repcoverage-{{ fastq_name }}.err
module load samtools
module load bowtie2
module load bedtools

cd /users/yjz/data/yjz/all_consensus_transcription_factors

#gunzip {{ fastq_name_gz }}
wait
echo {{ fastq_name }}.fastq > filelist_{{ fastq_name }}.txt
# run repcoverage? for all 4 repeat groups
python /users/yjz/data/yjz/repcoverage/repcoverage.py --consensus_dir /users/yjz/data/yjz/repcoverage/data/group1 \
    --genome_index /users/yjz/data/yjz/repcoverage/data/group1/index \
    --bowtie_options ' --no-unal --sensitive-local -p 16 ' \
    --output output_group1 output_group1 filelist_{{ fastq_name }}.txt 
python /users/yjz/data/yjz/repcoverage/repcoverage.py --consensus_dir /users/yjz/data/yjz/repcoverage/data/group2 \
    --genome_index /users/yjz/data/yjz/repcoverage/data/group2/index \
    --bowtie_options ' --no-unal --sensitive-local -p 16 ' \
    --output output_group2 output_group2 filelist_{{ fastq_name }}.txt 
python /users/yjz/data/yjz/repcoverage/repcoverage.py --consensus_dir /users/yjz/data/yjz/repcoverage/data/group3 \
    --genome_index /users/yjz/data/yjz/repcoverage/data/group3/index \
    --bowtie_options ' --no-unal --sensitive-local -p 16 ' \
    --output output_group3 output_group3 filelist_{{ fastq_name }}.txt 
python /users/yjz/data/yjz/repcoverage/repcoverage.py --consensus_dir /users/yjz/data/yjz/repcoverage/data/group4 \
    --genome_index /users/yjz/data/yjz/repcoverage/data/group4/index \
    --bowtie_options ' --no-unal --sensitive-local -p 16 ' \
    --output output_group4 output_group4 filelist_{{ fastq_name }}.txt 
python /users/yjz/data/yjz/repcoverage/repcoverage.py --consensus_dir /users/yjz/data/yjz/repcoverage/data/group5 \
    --genome_index /users/yjz/data/yjz/repcoverage/data/group5/index \
    --bowtie_options ' --no-unal --sensitive-local -p 16 ' \
    --output output_group5 output_group5 filelist_{{ fastq_name }}.txt 


# removing bam files
rm output_group1/{{ fastq_name }}.bam*
rm output_group2/{{ fastq_name }}.bam*
rm output_group3/{{ fastq_name }}.bam*
rm output_group4/{{ fastq_name }}.bam*
rm output_group5/{{ fastq_name }}.bam*

# now, get total mapping reads
python /users/yjz/data/yjz/repcoverage/total_mapping_bowtie.py {{ fastq_name }}.fastq total_mapping_output {{ fastq_name }}_total_mapping.txt

# now, divide table values by total mapping reads
python /users/yjz/data/yjz/repcoverage/process_table.py output_group1/{{ fastq_name }}.tsv \
    {{ fastq_name }}_total_mapping.txt output_group1/{{ fastq_name }}_cpm.tsv
python /users/yjz/data/yjz/repcoverage/process_table.py output_group2/{{ fastq_name }}.tsv \
    {{ fastq_name }}_total_mapping.txt output_group2/{{ fastq_name }}_cpm.tsv
python /users/yjz/data/yjz/repcoverage/process_table.py output_group3/{{ fastq_name }}.tsv \
    {{ fastq_name }}_total_mapping.txt output_group3/{{ fastq_name }}_cpm.tsv
python /users/yjz/data/yjz/repcoverage/process_table.py output_group4/{{ fastq_name }}.tsv \
    {{ fastq_name }}_total_mapping.txt output_group4/{{ fastq_name }}_cpm.tsv
python /users/yjz/data/yjz/repcoverage/process_table.py output_group5/{{ fastq_name }}.tsv \
    {{ fastq_name }}_total_mapping.txt output_group5/{{ fastq_name }}_cpm.tsv

rm {{ fastq_name }}.fastq
rm filelist_{{ fastq_name }}.txt
mv {{ fastq_name }}_total_mapping.txt total_mapping_output/
