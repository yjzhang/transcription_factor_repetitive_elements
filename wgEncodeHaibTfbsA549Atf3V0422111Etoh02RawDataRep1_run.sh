#!/bin/bash

# Request a lot more than an hour of time...
#SBATCH --time=2:00:00
# Use 1 nodes with 16
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH  --mem=16G
# Specify a job name:
#SBATCH -J run_repcoverage-1
# Specify an output file
#SBATCH -o run_repcoverage-1.out
#SBATCH -e run_repcoverage-1.err
module load samtools
module load bowtie2
module load bedtools

#wget -O wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1.fastq.gz http://hgdownload.cse.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeHaibTfbs/wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1.fastq
#gunzip wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1.fastq.gz
wait
echo wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1.fastq > filelist.txt
# run repcoverage? for all 4 repeat groups
#
#python /users/yjz/data/yjz/repcoverage/repcoverage.py --consensus_dir /users/yjz/data/yjz/repcoverage/data/group1 \
#    --genome_index /users/yjz/data/yjz/repcoverage/data/group1/index \
#    --output output_group1 output_group1 filelist.txt 
#python /users/yjz/data/yjz/repcoverage/repcoverage.py --consensus_dir /users/yjz/data/yjz/repcoverage/data/group2 \
#    --genome_index /users/yjz/data/yjz/repcoverage/data/group2/index \
#    --output output_group2 output_group2 filelist.txt 
#python /users/yjz/data/yjz/repcoverage/repcoverage.py --consensus_dir /users/yjz/data/yjz/repcoverage/data/group3 \
#    --genome_index /users/yjz/data/yjz/repcoverage/data/group3/index \
#    --output output_group3 output_group3 filelist.txt 
#python /users/yjz/data/yjz/repcoverage/repcoverage.py --consensus_dir /users/yjz/data/yjz/repcoverage/data/group4 \
#    --genome_index /users/yjz/data/yjz/repcoverage/data/group4/index \
#    --output output_group4 output_group4 filelist.txt 
#python /users/yjz/data/yjz/repcoverage/repcoverage.py --consensus_dir /users/yjz/data/yjz/repcoverage/data/group5 \
#    --genome_index /users/yjz/data/yjz/repcoverage/data/group5/index \
#    --output output_group5 output_group5 filelist.txt 


# removing bam files
rm output_group1/*.bam*
rm output_group2/*.bam*
rm output_group3/*.bam*
rm output_group4/*.bam*
rm output_group5/*.bam*

# now, get total mapping reads
python /users/yjz/data/yjz/repcoverage/total_mapping_bowtie.py wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1.fastq total_mapping_output total_mapping.txt

# now, divide table values by total mapping reads
python /users/yjz/data/yjz/repcoverage/process_table.py output_group1/wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1.tsv total_mapping.txt output_group1/wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1_cpm.tsv
python /users/yjz/data/yjz/repcoverage/process_table.py output_group2/wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1.tsv total_mapping.txt output_group2/wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1_cpm.tsv
python /users/yjz/data/yjz/repcoverage/process_table.py output_group3/wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1.tsv total_mapping.txt output_group3/wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1_cpm.tsv
python /users/yjz/data/yjz/repcoverage/process_table.py output_group4/wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1.tsv total_mapping.txt output_group4/wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1_cpm.tsv
python /users/yjz/data/yjz/repcoverage/process_table.py output_group5/wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1.tsv total_mapping.txt output_group5/wgEncodeHaibTfbsA549Atf3V0422111Etoh02RawDataRep1_cpm.tsv

# remove fastq?
